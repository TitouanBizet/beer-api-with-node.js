'use strict';


const Lab = require('@hapi/lab');
const {expect} = require('@hapi/code');
const {afterEach, beforeEach, describe, it} = exports.lab = Lab.script();
const {init} = require('../src/server.js');
const STR = require('../config/messages.js');

const brasserie1 = {id: 123456, name: 'Test', city: 'Saint-Test'};
const brasserie2 = {id: 123456, name: 'Test', city: 'Test DC'};

const brewRequest = (method, token=null) => ({
	method: method,
	url: '/api/v1/brasserie'.concat(['put', 'delete'].includes(method) ? '/123456' : ''),
	payload: brasserie1,
	headers: {Authorization: token}
});

const tokenRequest = {method: 'get', url: '/api/v1/generate/1/TB'};

// Brasseries

describe('Token - GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });
	
    it('Token', async () => {
        const res = await server.inject(tokenRequest);
		
        expect(res.statusCode).to.equal(200);
    });
})

describe('Brasserie - GET /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		await server.inject(brewRequest('post', token));
		token = (await server.inject(tokenRequest)).result;
		
		await server.inject(brewRequest('post', token));
    });

    afterEach(async () => {
		await server.inject(brewRequest('delete', token));
		
        await server.stop();
    });

    it('GET correct ', async () => {
        const res = await server.inject(brewRequest('get'));
		
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.once.include({id: 1, name: '(512) Brewing Company', city: 'Austin'});
        expect(res.result).to.once.include({id: 1423, name: 'Brasserie du Bouffay', city: 'Carquefou'});
    });


    it('URL non gérée / ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(STR.NOT_FOUND);
    });
	
    it('URL non gérée /api/v1/brasserie/ ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(STR.NOT_FOUND);
    });

    it('Recupération de la brasserie d’id 123456 ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/123456'
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(brasserie1);
    });

    it('Récupération d’une brasserie inexistante ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie/654321'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(STR.BREW.NOT_FOUND);
    });
});

describe('Brasserie - PUT /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		await server.inject(brewRequest('post', token));
		token = (await server.inject(tokenRequest)).result;
    });

    afterEach(async () => {
        await server.inject(brewRequest('delete', token));
		
        await server.stop();
    });

    it('Modification sans payload ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/123456',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD);
    });
	
    it('Modification avec un payload non conforme ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/123456',
            payload: {chat: 44},
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD);
    });

    it('Pas de header', async () => {
        let res = await server.inject(brewRequest('put'));
        expect(res.statusCode).to.equal(401);
        expect(res.result).to.equal(STR.MISSING_AUTHORIZATION);
    });

    it('Identité', async () => {
        let res = await server.inject(brewRequest('put', token));
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(brasserie1);
		
        res = await server.inject(brewRequest('get'));

        expect(res.result).to.once.include(brasserie1);
    });

    it('Modification fonctionnelle Saint-Test -> Test DC', async () => {		
        let res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/123456',
            payload : brasserie2,
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(brasserie2);
		
		
        res = await server.inject(brewRequest('get'));

        expect(res.result).to.once.include(brasserie2);
    });

    it('Modification non existant ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/654321',
            payload : {id: 654321, name: 'Test', city: 'ville'},
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal(STR.BREW.NOT_EXISTS);
    });
	
	it('Payload ≠ Param ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/brasserie/123456',
            payload : {id: 654321, name: 'Test', city: 'ville'},
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.NOT_EQUAL);
    });
});

describe('Brasserie - POST /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		await server.inject(brewRequest('post', token));
		token = (await server.inject(tokenRequest)).result;
    });

    afterEach(async () => {
		await server.inject(brewRequest('delete', token));
		
        await server.stop();
    });

    it("Ajout correct ", async () => {
		await server.inject(brewRequest('delete', token));
		
        let res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
            payload: brasserie2,
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal(brasserie2)
		
		res = await server.inject(brewRequest('get'))
		
        expect(res.result).to.once.include(brasserie2);
    });	

    it('Pas de header', async () => {
        let res = await server.inject(brewRequest('post'));
        expect(res.statusCode).to.equal(401);
        expect(res.result).to.equal(STR.MISSING_AUTHORIZATION);
    });
	
    it("Ajout existant ", async () => {
        const res = await server.inject(brewRequest('post', token));

        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal(STR.BREW.ALREADY_EXISTS)
    });
	
    it("Ajout sans payload", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD)
    });
	
    it("Ajout avec mauvais payload", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie',
            payload: {},
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD)
    });

    it('Populate ', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/brasserie/populate',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({message: "Populate done"});
    });
});

describe('Brasserie - DELETE /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		token = (await server.inject(tokenRequest)).result;
		
		await server.inject(brewRequest('post', token));
    });

    afterEach(async () => {
        await server.inject(brewRequest('delete', token));
		
        await server.stop();
    });

    it('Pas de header', async () => {
        let res = await server.inject(brewRequest('delete'));
        expect(res.statusCode).to.equal(401);
        expect(res.result).to.equal(STR.MISSING_AUTHORIZATION);
    });

    it('Suppression ok ', async () => {
        let res = await server.inject(brewRequest('delete', token));
		
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(brasserie1);
		
		res = await server.inject({
			method: 'get',
			url: '/api/v1/brasserie/123456'
		})
		
		expect(res.statusCode).to.equal(404);
    });
	
    it('Suppression ko id inexistant ', async () => {
        const res = await server.inject({
            method: 'delete',
            url: '/api/v1/brasserie/654321',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal(STR.BREW.NOT_EXISTS);
    });
	
    it('Paramètre incorrect ', async () => {
        const res = await server.inject({
            method: 'delete',
            url: '/api/v1/brasserie/LLL',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PARAMS);
    });
});




// Bières

const biere1 = {id: 123456, name: 'Test', state: 'Testie', breweryId: 1};
const biere2 = {id: 123456, name: 'Test', state: 'Test land', breweryId: 1};

const beerRequest = (method, token=null) => ({
	method: method,
	url: '/api/v1/biere'.concat(['put', 'delete'].includes(method) ? '/123456' : ''),
	payload: biere1,
	headers: {Authorization: token}
});

describe('Bière - GET /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		await server.inject(beerRequest('post', token));
		token = (await server.inject(tokenRequest)).result;
		
		await server.inject(beerRequest('post', token));
    });

    afterEach(async () => {
		await server.inject(beerRequest('delete', token));
		
        await server.stop();
    });

    it('GET correct ', async () => {
        const res = await server.inject(beerRequest('get'));
		
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.once.include({id: 1, name: 'Hocus Pocus', state: 'Vermont', breweryId: 812});
        expect(res.result).to.once.include({id: 5914, name:'Ambr', state: '', breweryId: 1423});
    });

    it('URL non gérée / ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(STR.NOT_FOUND);
    });
	
    it('URL non gérée /api/v1/biere/ ', async () => {
        const res = await server.inject({
            url: '/api/v1/biere/'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(STR.NOT_FOUND);
    });

    it('Recupération de la biere d’id 123456 ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/123456'
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(biere1);
    });

    it('Récupération d’une biere inexistante ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/v1/biere/654321'
        });
        expect(res.statusCode).to.equal(404);
        expect(res.result).to.equal(STR.BEER.NOT_FOUND);
    });
});

describe('Bière - PUT /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		await server.inject(beerRequest('post', token));
		token = (await server.inject(tokenRequest)).result;
    });

    afterEach(async () => {
        await server.inject(beerRequest('delete', token));
		
        await server.stop();
    });

    it('Modification sans payload ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/123456',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD);
    });
	
    it('Modification avec un payload non conforme ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/123456',
            payload: {chat: 44},
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD);
    });

    it('Pas de header', async () => {
        let res = await server.inject(beerRequest('put'));
        expect(res.statusCode).to.equal(401);
        expect(res.result).to.equal(STR.MISSING_AUTHORIZATION);
    });

    it('Identité', async () => {
        let res = await server.inject(beerRequest('put', token));
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(biere1);
		
        res = await server.inject(beerRequest('get'));

        expect(res.result).to.once.include(biere1);
    });

    it('Modification fonctionnelle Saint-Test -> Test DC', async () => {		
        let res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/123456',
            payload : biere2,
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(biere2);
		
		
        res = await server.inject(beerRequest('get'));

        expect(res.result).to.once.include(biere2);
    });

    it('Modification non existant ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/654321',
            payload : {id: 654321, name: 'Test', state: 'Testie', breweryId: 1},
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal(STR.BEER.NOT_EXISTS);
    });
	
	it('Payload ≠ Param ', async () => {
        const res = await server.inject({
            method: 'put',
            url: '/api/v1/biere/123456',
            payload : {id: 654321, name: 'Test', state: 'Testie', breweryId: 1},
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.NOT_EQUAL);
    });
});

describe('Bière - POST /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		await server.inject(beerRequest('post', token));
		token = (await server.inject(tokenRequest)).result;
    });

    afterEach(async () => {
		await server.inject(beerRequest('delete', token));
		
        await server.stop();
    });

    it('Pas de header', async () => {
        let res = await server.inject(beerRequest('post'));
        expect(res.statusCode).to.equal(401);
        expect(res.result).to.equal(STR.MISSING_AUTHORIZATION);
    });

    it("Ajout correct ", async () => {
		await server.inject(beerRequest('delete', token));
		
        let res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: biere2,
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal(biere2)
		
		res = await server.inject(beerRequest('get'))
		
        expect(res.result).to.once.include(biere2);
    });
	
    it("Ajout existant ", async () => {
        const res = await server.inject(beerRequest('post', token));

        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal(STR.BEER.ALREADY_EXISTS)

    });

    it("Ajout sans payload", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD)

    });
	
    it("Ajout avec mauvais payload", async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere',
            payload: {},
			headers: {Authorization: token}
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PAYLOAD)
    });

    it('Populate ', async () => {
        const res = await server.inject({
            method: 'post',
            url: '/api/v1/biere/populate',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result).to.equal({message: "Populate done"});
    });
});

describe('Bière - DELETE /', () => {
    let server, token;

    beforeEach(async () => {
        server = await init();
		
		token = (await server.inject(tokenRequest)).result;
		
		await server.inject(beerRequest('post', token));
    });

    afterEach(async () => {
        await server.inject(beerRequest('delete', token));
		
        await server.stop();
    });

    it('Pas de header', async () => {
        let res = await server.inject(beerRequest('delete'));
        expect(res.statusCode).to.equal(401);
        expect(res.result).to.equal(STR.MISSING_AUTHORIZATION);
    });

    it('Suppression ok ', async () => {
        let res = await server.inject(beerRequest('delete', token));
		
        expect(res.statusCode).to.equal(200);
        expect(res.result).to.equal(biere1);
		
		res = await server.inject({
			method: 'get',
			url: '/api/v1/biere/123456',
			headers: {Authorization: token}
		})
		
		expect(res.statusCode).to.equal(404);
    });
	
    it('Suppression ko id inexistant ', async () => {
        const res = await server.inject({
            method: 'delete',
            url: '/api/v1/biere/654321',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(203);
        expect(res.result).to.equal(STR.BEER.NOT_EXISTS);
    });
	
    it('Paramètre incorrect ', async () => {
        const res = await server.inject({
            method: 'delete',
            url: '/api/v1/biere/LLL',
			headers: {Authorization: token}
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result).to.equal(STR.INVALID_PARAMS);
    });
});
