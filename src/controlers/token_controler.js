'use strict';

const {sign} = require("jsonwebtoken");
const key = require("../../config/config.js").myKey;

module.exports = {
	sign: async function(request, h) {
		
		const token = sign({
			id: request.params.id,
			name: request.params.name
		}, key, {algorithm: 'HS256', expiresIn: '1h'});
		
		return h.response(token).code(200);
	}
}