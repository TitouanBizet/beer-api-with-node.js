'use strict';

const db = require("../model/index.js");
const {NOT_EQUAL, BREW} = require("../../config/messages.js");
const fileName = require("../../config/config.js").brewerieFileName;
const {parse} = require("csv-parse")
const readFile = require('fs').readFile;

module.exports = {
	getAll: async function(request, h) {
		const data = await db.Brewery.findAll({raw: true});
		return h.response(data).code(200);
	},
	getId: async function(request, h) {
		const data = await db.Brewery.findByPk(request.params.id, {raw: true});
		if (!data)
			return h.response(BREW.NOT_FOUND).code(404);
		else
			return h.response(data).code(200);
	},
	post: async function(request, h) {
		const payload = request.payload;
		
		if (await db.Brewery.findByPk(payload.id))
			return h.response(BREW.ALREADY_EXISTS).code(203);
		
		// Cas nominal
		db.Brewery.create(payload);
		return h.response(payload).code(201);
	},
	put: async function(request, h) {
		if (request.params.id != request.payload.id) {
			return h.response(NOT_EQUAL).code(400)
		} else if (!(await db.Brewery.findByPk(request.params.id))) {
			return h.response(BREW.NOT_EXISTS).code(203);
		} else {
			db.Brewery.update(request.payload, {where: {id: request.params.id}});
			return h.response(request.payload).code(200);
		}
	},
	delete: async function(request, h) {
		const deleted = await db.Brewery.findByPk(request.params.id, {raw: true});
		if (!deleted) {
			return h.response(BREW.NOT_EXISTS).code(203);
		} else {
			db.Brewery.destroy({where: {id: request.params.id}});
			return h.response(deleted).code(200);
		}
	},
	populate: async function(request, h) {
		await db.sequelize.sync();
		
		readFile(fileName, "utf-8", (err, data) => {
			if (err) {
				console.error(err);
			} else {
				parse(data, {
					columns: true,
					delimiter: ";",
					skip_empty_lines: true
				}, function(err, input) {
					if (err == undefined) {
						db.Brewery.bulkCreate(
							input.map(x => ({id: x.id, name: x.breweries, city: x.city})),
							{ignoreDuplicates: true}
						);
					} else {
						console.log(err);
					}
				});
			}
		});
		
		return h.response({message: "Populate done"}).code(201);
	}
}
