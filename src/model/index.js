const {Sequelize, DataTypes} = require("sequelize");
const parse = require("csv-parse")
const readFile = require('fs').readFile;

const conf = require("../../config/config.js");

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: conf.dbPath,
    logging: /*console.log/*/ false,
});

const db = {};
db.Brewery = require('./brewery')(sequelize,DataTypes);
db.Beer = require('./beer')(sequelize,DataTypes);

db.Brewery.hasMany(db.Beer, {
	as: "bieres",
	onDelete: 'cascade',
	onUpdate: 'cascade'
})

db.Beer.belongsTo(db.Brewery,{
    foreignKey: "breweryId",
    as: "brasserie",
    allowNull: false
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;

