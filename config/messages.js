// Messages d’erreurs

module.exports.NOT_FOUND = {error: 'Not Found', message: 'Not Found', statusCode: 404};
module.exports.NOT_EQUAL = {error: "Bad Request", message: "params.id ≠ payload.id"};
module.exports.INVALID_PAYLOAD = {error: "Bad Request", message: "Invalid request payload input", statusCode: 400};
module.exports.INVALID_PARAMS = {error: "Bad Request", message: "Invalid request params input", statusCode: 400};
module.exports.MISSING_AUTHORIZATION = {error: 'Unauthorized', message: 'Missing authentication', statusCode: 401};

module.exports.BREW = {
	NOT_FOUND: {error: "Not Found", message: "Brewery not found"},
	ALREADY_EXISTS: {error: "Already Exists", message: "A brewery with the same id already exists"},
	NOT_EXISTS: {error: "Not Exists", message: "No brewery with this id exists"}
}

module.exports.BEER = {
	NOT_FOUND: {error: "Not Found", message: "Beer not found"},
	ALREADY_EXISTS: {error: "Already Exists", message: "A beer with the same id already exists"},
	NOT_EXISTS: {error: "Not Exists", message: "No beer with this id exists"}
}