# Beer API with Node.js

## Description
I have developed this small project with [Timothé Béziau--Jecty](https://github.com/Caale179) during my second year of computer science DUT (a French 2 year-long degree). The goal was to create a REST API using Node.js and a catalog of beers and breweries providen by an open data source I have forgiven (I’m terribly confused). The code is in English but the documentation is written in French.

## Installation
You need Node.js to run this project. In root directory, run:
> npm install

## Usage
To launch the application:

> npm start

To run unit tests:

> npm test

The unit tests were all ok when the project was developed (in 2021), but now some leaks have been found in dev dependencies, also tests will always be marked as failed even if all suite has passed.

## Authors and acknowledgment
Thanks Timothé Béziau--Jecty who made a consequent part of the job!

## License
[CeCILL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html) (BSD like)
