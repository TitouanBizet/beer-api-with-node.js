const path = require('path');
const myKey = '1234'; // Never Share your secret key
const dir = "bd"

module.exports.myKey = myKey;
module.exports.brewerieFileName	= path.join("bd", 'open-beer-database-breweries.csv');
module.exports.beerFileName = path.join("bd", 'open-beer-database.csv');
module.exports.dbPath = path.join("bd", "database.sqlite");