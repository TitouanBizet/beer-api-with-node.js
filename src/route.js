const Joi = require('joi');

const schemaBrewery = Joi.object({
    id: Joi.number(),
    nameBreweries: Joi.string(),
    city : Joi.string()
});

const schemaBreweries = Joi.array().items(schemaBrewery);

const brewerieControleur = require("./controlers/breweries_controler.js");
const beerControleur = require("./controlers/beer_controler.js");
const tokenControleur = require("./controlers/token_controler.js");

const STR = require("../config/messages.js");

const routes = [
    {
        method: 'GET',
        path: '/api/v1/brasserie',
        options: {
            description: 'Obtient la liste de toutes les brasseries.',
            notes: 'Renvoie un tableau de brasseries.',
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default([
								{id: 165, name: 'Brasserie Bnifontaine', city: 'Bnifontaine'},
                                {id: 177, name: 'Brasserie De Saint Sylvestre', city: 'St-Sylvestre-Cappel'}
							])
                        }
                    },
                }
            },

            tags: ['api'],
			handler: brewerieControleur.getAll
        }

    }, {
        method: 'GET',
        path: '/api/v1/brasserie/{id}',
        options: {
            description: 'Obtient une brasserie grâce à son id.',
            notes: 'Renvoie un objet brasserie (id, name, city).',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de la brasserie à récupérer.")
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema : schemaBreweries.default({id: 165, name: 'Brasserie Bnifontaine', city: 'Bnifontaine'})
                        },
						'400': {
							description: 'Mauvais format d’identifiant',
							schema: Joi.object(STR.INVALID_PARAMS)
						}
                    },
                }
            },

            tags: ['api'],
			handler: brewerieControleur.getId
        }

    }, {
		method: 'POST',
		path: '/api/v1/brasserie',
		options: {
            description: 'Ajoute une brasserie.',
            notes: 'Renvoie la brasserie ajoutée.',
			validate: {
				payload: Joi.object({
					id: Joi.number().required().example(1),
					name: Joi.string().required().example("Brasserie"),
					city: Joi.string().required().example("Nantes")
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default({id: 165, name: 'Brasserie Bnifontaine', city: 'Bnifontaine'})
                        },
						'203': {
							description: 'Un item au même id existe',
							schema: Joi.object(STR.BREW.ALREADY_EXISTS)
						},
						'400': {
							description: 'Mauvais format de payload',
							schema: Joi.object(STR.INVALID_PAYLOAD)
						},
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: brewerieControleur.post,
			auth: 'jwt'
		}
	}, {
		method: 'POST',
        path: '/api/v1/brasserie/populate',
        options: {
            description: 'Remplit la base de brasseries.',
            notes: 'Remplit à partir d’un fichier csv.',
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            description: 'Bonne requête',
							schema: Joi.object({message: "Populate done"})
                        },
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: brewerieControleur.populate,
			auth: 'jwt'
        }

	}, {
		method: 'PUT',
		path: '/api/v1/brasserie/{id}',
		options: {
            description: 'Modifie une brasserie.',
            notes: 'Renvoie la brasserie modifiée.',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de la brasserie à modifier.")
				}),
				payload: Joi.object({
					id: Joi.number().required().example(1),
					name: Joi.string().required().example("Brasserie"),
					city: Joi.string().required().example("Nantes")
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default({id: 165, name: 'Brasserie Bnifontaine', city: 'Bnifontaine'})
                        },
						'404': {
							description: 'Id inconnu',
							schema: Joi.object(STR.BEER.NOT_FOUND)
						},
						'400': {
							description: 'Mauvais payload ou id',
							schema: Joi.object({error: "Bad Request"})
						},
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: brewerieControleur.put,
			auth: 'jwt'
		}
	}, {
		method: 'DELETE',
		path: '/api/v1/brasserie/{id}',
		options: {
            description: 'Supprime une brasserie.',
            notes: 'Renvoie la brasserie supprimée.',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de la brasserie à supprimer.")
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default({id: 165, name: 'Brasserie Bnifontaine', city: 'Bnifontaine'})
                        },
						'404': {
							description: 'Id inconnu',
							schema: Joi.object(STR.BEER.NOT_FOUND)
						},
						'400': {
							description: 'Mauvais paramètre',
							schema: Joi.object(STR.INVALID_PARAMS)
						},
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: brewerieControleur.delete,
			auth: 'jwt'
		}
	}, {
        method: 'GET',
        path: '/api/v1/biere',
        options: {
            description: 'Obtient la liste de toutes les bières.',
            notes: 'Renvoie un tableau de bières.',
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default([
								{id: 165, name: 'Bretzbeer', state: 'Düsseldorf'},
                                {id: 177, name: 'Brasserie De Saint Sylvestre', state: 'St-Sylvestre-Cappel'}
							])
                        }
                    },
                }
            },

            tags: ['api'],
			handler: beerControleur.getAll
        }

    }, {
        method: 'GET',
        path: '/api/v1/biere/{id}',
        options: {
            description: 'Obtient une bière grâce à son id.',
            notes: 'Renvoie un objet bière (id, name, state).',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de la bière à récupérer.")
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema : schemaBreweries.default({id: 165, name: 'Bretzbeer', state: 'Düsseldorf'})
                        },
						'400': {
							description: 'Mauvais format d’identifiant',
							schema: Joi.object(STR.INVALID_PARAMS)
						}
                    },
                }
            },

            tags: ['api'],
			handler: beerControleur.getId
        }

    }, {
		method: 'POST',
		path: '/api/v1/biere',
		options: {
            description: 'Ajoute une bière.',
            notes: 'Renvoie la bière ajoutée.',
			validate: {
				payload: Joi.object({
					id: Joi.number().required().example(1),
					name: Joi.string().required().example("Bierre au beurre"),
					state: Joi.string().required().example("France"),
					breweryId: Joi.number().required().example(5)
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default({id: 165, name: 'Bretzbeer', state: 'Düsseldorf'})
                        },
						'203': {
							description: 'Un item au même id existe',
							schema: Joi.object(STR.BEER.ALREADY_EXISTS)
						},
						'400': {
							description: 'Mauvais format de payload',
							schema: Joi.object(STR.INVALID_PAYLOAD)
						},
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: beerControleur.post,
			auth: 'jwt'
		}
	}, {
		method: 'POST',
        path: '/api/v1/biere/populate',
        options: {
            description: 'Remplit la base de bières.',
            notes: 'Renvoie 200.',
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '201': {
                            description: 'Bonne requête',
							schema: Joi.object({message: "Populate done"})
                        },
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: beerControleur.populate,
			auth: 'jwt'
        }

	}, {
		method: 'PUT',
		path: '/api/v1/biere/{id}',
		options: {
            description: 'Modifie une bière.',
            notes: 'Renvoie la bière modifiée.',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de la bière à modifier.")
				}),
				payload: Joi.object({
					id: Joi.number().required().example(1),
					name: Joi.string().required().example("Bierre au beurre"),
					state: Joi.string().required().example("France"),
					breweryId: Joi.number().required().example(5)
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default({id: 165, name: 'Bretzbeer', state: 'Düsseldorf'})
                        },
						'404': {
							description: 'Id inconnu',
							schema: Joi.object(STR.BEER.NOT_FOUND)
						},
						'400': {
							description: 'Mauvais payload ou id',
							schema: Joi.object({error: "Bad Request"})
						},
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: beerControleur.put,
			auth: 'jwt'
		}
	}, {
		method: 'DELETE',
		path: '/api/v1/biere/{id}',
		options: {
            description: 'Supprime une bière.',
            notes: 'Renvoie la bière supprimée.',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de la bière à supprimer.")
				})
			},
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            description: 'Bonne requête',
                            schema: schemaBreweries.default({id: 165, name: 'Bretzbeer', state: 'Düsseldorf'})
                        },
						'404': {
							description: 'Id inconnu',
							schema: Joi.object(STR.BEER.NOT_FOUND)
						},
						'400': {
							description: 'Mauvais paramètre',
							schema: Joi.object(STR.INVALID_PARAMS)
						},
						'401': {
							description: 'Token manquant ou incorrect',
							schema: Joi.object(STR.MISSING_AUTHORIZATION)
						}
                    },
                }
            },

            tags: ['api'],
			handler: beerControleur.delete,
			auth: 'jwt'
		}
	}, {
		method: 'GET',
		path: '/api/v1/generate/{id}/{name}',
		options: {
			description: 'Génère un webtoken.',
			notes: 'Renvoie un webtoken encodant {id: id, name: name}',
			validate: {
				params: Joi.object({
					id: Joi.number().min(0).description("L’id de l’utilisateur."),
					name: Joi.string().description("Le nom de l’utilisateur.")
				})
			},
			plugins: {
				'hapi-swagger': {
					responses: {
						'200': {
                            description: 'Bonne requête',
                            schema: Joi.string().example('lKJmlLjmlkJUigUIgl')
                        },
						'400': {
							description: 'Mauvais paramètre',
							schema: Joi.object(STR.INVALID_PARAMS)
						}
					}
				}
			},
		
			tags: ['api'],
			handler: tokenControleur.sign
		}
	}
];

module.exports = routes;
