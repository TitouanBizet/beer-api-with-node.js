'use strict';
const Hapi = require('@hapi/hapi');

const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');

const AuthJwt = require('hapi-auth-jwt2');

const conf = require("../config/config.js");
const users = require("./model/token.js");
	
const swaggerOptions = {
	info: {
		title: 'Breweries API Documentation',
		version: '1.0.0'
	},
	debug: true
};

const getServer = async function() {
	const server = Hapi.server({
		port: 3000,
		host: 'localhost'
	});

	await server.register([
		{plugin: AuthJwt},
		{plugin: Inert},
		{plugin: Vision},
		{plugin: HapiSwagger, options: swaggerOptions}
	]);

	server.auth.strategy('jwt', 'jwt', {
		key: conf.myKey,
		validate: (decoded, request, h) => ({isValid: users[decoded.id] && users[decoded.id].name == decoded.name})
	});
	
	server.route(require('./route'));
	
	return server;
}


module.exports.init = async () => {
	const server = await getServer();
	
	await server.initialize();
	return server;
};

module.exports.start = async () => {
	const server = await getServer();

	const token = (await server.inject({
		method: 'GET',
		url: '/api/v1/generate/1/TB',
	})).result;

	await server.inject({
		method: 'POST',
		url: '/api/v1/brasserie/populate',
		headers: {Authorization: token}
	});
	await server.inject({
		method: 'POST',
		url: '/api/v1/biere/populate',
		headers: {Authorization: token}
	});
	console.log('-- Populate done --');


	await server.start();

	console.log(`Server running at: ${server.info.uri}`);
	return server;
};

process.on('unhandledRejection', (err) => {
	console.log(err);
	process.exit(1);
});
